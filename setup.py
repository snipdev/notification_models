from setuptools import setup, find_packages
import os

setup(
    name='notification-models',
    author='snip',
    url='snip.today',
    author_email='info@snip.today',
    packages=find_packages(),
    install_requires=[
        'django==1.11.12',
        'psycopg2==2.7.3.2',
        'django-model-utils==3.0.0',
    ],
    include_package_data=True,
    version='0.1.07',
    description='Notification models used by Readers & Creators web apps',
)
# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

