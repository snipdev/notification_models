from django.apps import AppConfig


class NotificationModelsConfig(AppConfig):
    name = 'notification_models'
