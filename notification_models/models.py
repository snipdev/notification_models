from django.db import models
from snippet_models.models import Post
from django.contrib.auth.models import User


class AbstractOwner(models.Model):
    user = models.ForeignKey(User, null=True, blank=True, db_index=True, on_delete=models.CASCADE)
    session = models.CharField(max_length=32, null=True, blank=True, db_index=True)

    class Meta:
        abstract = True


class Token(AbstractOwner):
    date = models.DateTimeField(auto_now_add=True, db_index=True)
    token = models.CharField(max_length=200, unique=True)
    platform = models.CharField(max_length=10)


class Topic(models.Model):
    MAX_NAME_LENGTH = 100
    MAX_DESCRIPTION_LENGTH = 150

    name = models.CharField(max_length=MAX_NAME_LENGTH)
    description = models.CharField(max_length=MAX_DESCRIPTION_LENGTH, null=True)


class TopicSubscriptions(AbstractOwner):
    topic = models.ForeignKey(Topic, db_index=True, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True, db_index=True)
    active = models.BooleanField(default=True)


class Notification(models.Model):
    MAX_BODY_LENGTH = 500
    MAX_TITLE_LENGTH = 200

    topic = models.ForeignKey(Topic, db_index=True, null=True, on_delete=models.CASCADE)
    target = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True, db_index=True)
    title = models.CharField(max_length=MAX_TITLE_LENGTH)
    body = models.CharField(max_length=MAX_BODY_LENGTH, null=True)
    url = models.URLField(null=True)
    image_url = models.URLField(null=True)
    post = models.ForeignKey(Post, null=True, on_delete=models.CASCADE)


class InteractionLog(AbstractOwner):
    notification = models.ForeignKey(Notification, db_index=True, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True, db_index=True)
    action = models.CharField(max_length=20)


class Unsubscribe(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
